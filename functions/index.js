const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

const FieldValue = require("firebase-admin").firestore.FieldValue;

const firestore = admin.firestore();

// Create a new function which is triggered on changes to /status/{uid}
// Note: This is a Realtime Database trigger, *not* Cloud Firestore.
exports.onUserStatusChanged = functions.database.ref('/customers/{customerId}/users/{userId}').onWrite(
    async (change, context) => {
        const eventStatus = change.after.val();

        console.log(`here ${JSON.stringify(eventStatus)}`);

        let customerId = context.params.customerId;
        let userId = context.params.userId;

        // console.log(`here customerId:${customerId}, userId:${userId}`);

        const userStatusFirestoreRef = firestore.doc(`customers/${customerId}/users/${userId}`);

        // It is likely that the Realtime Database change that triggered
        // this event has already been overwritten by a fast change in
        // online / offline status, so we'll re-read the current data
        // and compare the timestamps.
        const statusSnapshot = await change.after.ref.once('value');
        const status = statusSnapshot.val();

        console.log(`here 2 ${status.online.last_changed}`);
        // If the current timestamp for this data is newer than
        // the data that triggered this event, we exit this function.
        if (status.last_changed > eventStatus.last_changed) {
            return null;
        }

        // Otherwise, we convert the last_changed field to a Date
        eventStatus.online.last_changed = FieldValue.serverTimestamp();

        // ... and write it to Firestore.
        return userStatusFirestoreRef.set(eventStatus, {merge: true});
    });


exports.onCustomerStatusChanged = functions.database.ref('/customers/{customerId}/online').onWrite(
    async (change, context) => {

        console.log('customer here');
        const eventStatus = change.after.val();

        console.log(`here customer ${JSON.stringify(eventStatus)}`);

        let customerId = context.params.customerId;

        const customerStatusFirestoreRef = firestore.doc(`customers/${customerId}`);

        const statusSnapshot = await change.after.ref.once('value');
        const status = statusSnapshot.val();
        if (status.last_changed > eventStatus.last_changed) {
            return null;
        }

        eventStatus.last_changed = FieldValue.serverTimestamp();

        let data = {online: eventStatus};

        return customerStatusFirestoreRef.set(data, {merge: true});
    });

exports.sendNotification = functions.firestore.document('customers/{customerId}/chats/{chatId}/messages/{messageId}').onCreate(
    async (messageSnapshot, context) => {
        console.log('<======== send notification START ============>');
        let messageData = messageSnapshot.data();
        let customerId = context.params.customerId;
        let isCustomerMessage = messageData.idFrom === customerId;

        if (isCustomerMessage) return null;

        //get customer online status
        let customerSnapshot = await  admin.firestore().doc(`customers/${customerId}`).get();
        let customerData = customerSnapshot.data();
        console.log(`customerInfo: ${JSON.stringify(customerData)}`);

        let isOnline = customerData.online.state === 'online';

        console.log(`new message content ${messageData.content} customerId: ${customerId} isCustomer: ${isCustomerMessage} isOnline: ${isOnline}`);

        if (!isOnline) {
            let pushToken = customerData.pushToken;
            let messageContent = messageData.content;
            let payload = {
                notification: {
                    title: 'New Message!',
                    body: messageContent,
                    badge: '1',
                    sound: 'default'
                }
            };
            await  admin.messaging().sendToDevice(pushToken, payload);
        }

        console.log('<======== send notification END ============>');
        return null;
    }
);

exports.createEntry = functions.https.onCall(async (data, context) => {
    if (data) {
        let clientId = data.clientId;
        let customerId = data.customerId;
        if (!clientId || !customerId) {
            return {error: "!clientId  || !customerId"};
        }
        let detailsPath = `customers/${customerId}/users/${clientId}/details`;
        let newEntryRef = firestore.collection(detailsPath).doc();
        let setResult = await newEntryRef.set({type: data.type, name: data.name, value: data.value}, {merge: true});
        console.log(`details path: ${detailsPath} new doc ID: ${newEntryRef.id}`);
        return {data: newEntryRef.id};
    } else {
        return {data: "error"};
    }
});

exports.updateEntry = functions.https.onCall(async (data, context) => {
    if (data) {
        let clientId = data.clientId;
        let customerId = data.customerId;
        let entryId = data.entryId;
        if (!clientId || !customerId || !entryId) {
            return {error: "!clientId  || !customerId || !entryId"};
        }
        let detailsPath = `customers/${customerId}/users/${clientId}/details`;
        let entryRef = firestore.collection(detailsPath).doc(entryId);
        let setResult = await entryRef.set({type: data.type, name: data.name, value: data.value}, {merge: true});
        console.log(`details path: ${detailsPath} new doc ID: ${entryRef.id}`);
        return {data: entryRef.id};
    } else {
        return {error: "error"};
    }
});

exports.setDetails = functions.https.onCall(async (data, context) => {
    if (data) {
        let clientId = data.clientId;
        let customerId = data.customerId;
        if (!clientId || !customerId) {
            return {error: "!clientId  || !customerId"};
        }
        let detailsPath = `customers/${customerId}/users/${clientId}/details`;


        // Get a new write batch
        let batch = db.batch();
        let location = data.location;
        if (location) {
            let locationRef = detailsPath.doc();
            batch.set(locationRef, {value: location, type: 'location'})
        }

        batch.commit();

        return {data: "success"};
    } else {
        return {error: "error"};
    }
});

